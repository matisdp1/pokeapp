export interface Stat {
    name: string;
    value: number;
}

export interface Ability {
    name: string;
    url: string;
}

export interface Details {
    id: number;
    abilities: Ability[];
    height: number;
    weight: number;
    image: string;
    stats: Stat[];
    types: string[];
}

export interface Pokemon {
    name: string;
    url: string;
    details?: Details | null;
}

export interface PokeAPIState {
    pokedexSize: number;
    nextFetchLink: string;
    pokemons: Pokemon[];
    loading: boolean;
}

export interface PokeTeamState {
    team: Pokemon[];
    favorites: Pokemon[];
}

export interface State {
    pokeAPI: PokeAPIState;
    pokeTeam: PokeTeamState;
}

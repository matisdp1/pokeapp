import React, { useEffect } from "react";
import { StyleSheet, LogBox } from "react-native";

import { createStackNavigator } from "@react-navigation/stack";
import { NavigationContainer } from "@react-navigation/native";

import { Provider } from "react-redux";
import store from "./redux/store";

import Tabs from "./navigation/Tabs";
import { Home, MyPokeTeam } from "./screens";

const Stack = createStackNavigator();

const App = () => {
    useEffect(() => {
        LogBox.ignoreAllLogs(); // Delete for see logs on the emulator screen
    }, []);

    return (
        <Provider store={store}>
            <NavigationContainer>
                <Stack.Navigator
                    screenOptions={{
                        headerShown: false,
                    }}
                    initialRouteName={"Home"}
                >
                    <Stack.Screen name="Home" component={Tabs} />
                    <Stack.Screen name="MyPokeTeam" component={MyPokeTeam} />
                </Stack.Navigator>
            </NavigationContainer>
        </Provider>
    );
};

export default App;

const styles = StyleSheet.create({});

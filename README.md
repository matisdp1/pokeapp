# <img src="./assets/icons/pokeball.png" alt="pokeball" width="30"/> PokeApp

## Simple React Native app for your Pokémons

### Features 🚀

-   Fetch Pokémons from [PokeAPI](https://pokeapi.co/)
-   Build your own **Poke-DreamTeam**

### Main tools used 🔧

-   Redux Toolkit
-   React Navigation
-   TypeScript

### To Do 🧪

-   [ ] Fix bad positioning of the bottom navigation tab on launch
-   [ ] Add failed API request handlers
-   [ ] Refactoring (especially styles)
-   [ ] Search Pokémon feature
-   [ ] Splash Screen

### [Demo Video](https://youtu.be/Tz2pNY0TBGI) 🔗

#### Screenshots 📷

<img src="./screenshots/scsh1.png" alt="scsh" height="500"/>
<img src="./screenshots/scsh2.png" alt="scsh" height="500"/>
<img src="./screenshots/scsh3.png" alt="scsh" height="500"/>

#### Bye! 🖖

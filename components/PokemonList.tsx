import React from "react";
import { FlatList, Text, View } from "react-native";
import LottieView from "lottie-react-native";

import PokemonCard from "./PokemonCard";
import { SIZES, lotties } from "../constants";

import { Pokemon } from "../type";

interface Props {
    pokemons: Pokemon[];
    loading?: boolean;
    extended?: boolean;
}

const PokemonList = ({ pokemons, loading, extended }: Props) => {
    const renderItem = ({ item }: { item: Pokemon }) => (
        <PokemonCard pokemon={item} extended={extended} />
    );

    return (
        <View
            style={{
                marginHorizontal: SIZES.padding,
                padding: SIZES.padding,
                borderRadius: SIZES.radius,
                backgroundColor: "transparent",
            }}
        >
            {loading ? (
                <View style={{ width: 100, height: 100, alignSelf: "center" }}>
                    <LottieView
                        source={lotties.loading}
                        autoPlay
                        resizeMode="cover"
                    />
                </View>
            ) : (
                <FlatList
                    contentContainerStyle={{
                        marginTop: SIZES.base,
                        paddingHorizontal: 25, // Follow and AddToTeam Buttons Diameter / 2
                    }}
                    scrollEnabled={false}
                    data={pokemons}
                    keyExtractor={(item: Pokemon) => item.name}
                    renderItem={renderItem}
                    showsVerticalScrollIndicator={false}
                />
            )}
        </View>
    );
};

export default PokemonList;

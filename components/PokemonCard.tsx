import React, { useState, useEffect } from "react";
import { StyleSheet, Text, View, Image, TouchableOpacity } from "react-native";
import LottieView from "lottie-react-native";
import Icon from "react-native-vector-icons/MaterialIcons";

import { useDispatch, useSelector } from "react-redux";
import { addToTeam, removeFromTeam } from "../redux/features/pokeTeamSlice";

import { Pokemon, State } from "../type";

import {
    COLORS,
    SIZES,
    FONTS,
    lotties,
    globalStyles,
    icons,
} from "../constants";
import capitalize from "../util/capitalize";

interface Props {
    pokemon: Pokemon;
    extended?: boolean;
}

const PokemonCard = ({ pokemon, extended }: Props) => {
    const dispatch = useDispatch();

    const { team } = useSelector((state: State) => state.pokeTeam);

    const isInTeam =
        team.findIndex((teamPokemon) => teamPokemon.name === pokemon.name) ===
        -1
            ? false
            : true;

    const handleAddPress = () => {
        if (!isInTeam) {
            dispatch(addToTeam(pokemon));
            return;
        }
        dispatch(removeFromTeam(pokemon.name));
    };

    const renderDetails = () => {
        if (!pokemon.details) {
            return (
                <View style={{ width: 100, height: 100 }}>
                    <LottieView
                        source={lotties.loading}
                        autoPlay
                        resizeMode="cover"
                    />
                </View>
            );
        }

        return (
            <View
                style={{
                    width: "100%",
                    justifyContent: "center",
                    alignItems: "center",
                }}
            >
                <Text
                    style={{
                        textAlign: "center",
                        color: COLORS.white,
                        backgroundColor: COLORS.secondary,
                        borderRadius: SIZES.radius,
                        padding: SIZES.base,
                        ...FONTS.h4,
                    }}
                >
                    ID: {pokemon.details.id}
                </Text>
                {extended && (
                    <Text
                        style={{
                            color: COLORS.white,
                            marginVertical: SIZES.base,
                            ...FONTS.body4,
                        }}
                    >
                        {`Height: ${pokemon.details.height}  Weight: ${pokemon.details.weight}`}
                    </Text>
                )}
                <Image
                    source={{ uri: pokemon.details.image }}
                    style={{ width: "100%", height: 120 }}
                    resizeMode="contain"
                />
                {extended && (
                    <View>
                        <View
                            style={{
                                flexDirection: "row",
                                justifyContent: "center",
                                alignItems: "center",
                                marginBottom: SIZES.base,
                            }}
                        >
                            <Image
                                source={icons.heart}
                                style={{ width: 20, height: 20 }}
                                resizeMode="contain"
                            />
                            <Text
                                style={{
                                    marginLeft: SIZES.base,
                                    color: COLORS.white,
                                    ...FONTS.body3,
                                }}
                            >{`Health: ${pokemon.details.stats[0].value}`}</Text>
                        </View>
                        <View
                            style={{
                                flexDirection: "row",
                                justifyContent: "center",
                                alignItems: "center",
                                marginBottom: SIZES.base,
                            }}
                        >
                            <Image
                                source={icons.sword}
                                style={{ width: 20, height: 20 }}
                                resizeMode="contain"
                            />
                            <Text
                                style={{
                                    marginLeft: SIZES.base,
                                    color: COLORS.white,
                                    ...FONTS.body3,
                                }}
                            >{`Attack: ${pokemon.details.stats[1].value}`}</Text>
                        </View>
                        <View
                            style={{
                                flexDirection: "row",
                                justifyContent: "center",
                                alignItems: "center",
                                marginBottom: SIZES.base,
                            }}
                        >
                            <Image
                                source={icons.shield}
                                style={{ width: 20, height: 20 }}
                                resizeMode="contain"
                            />
                            <Text
                                style={{
                                    marginLeft: SIZES.base,
                                    color: COLORS.white,
                                    ...FONTS.body3,
                                }}
                            >{`Defense: ${pokemon.details.stats[2].value}`}</Text>
                        </View>
                    </View>
                )}
                <View
                    style={{
                        flexDirection: "row",
                        justifyContent: "center",
                    }}
                >
                    {pokemon.details.types.map((type) => (
                        <Text
                            style={{
                                textAlign: "center",
                                color: COLORS.white,
                                backgroundColor: COLORS.green,
                                padding: SIZES.base,
                                margin: SIZES.base,
                                borderRadius: SIZES.radius,
                                ...FONTS.h4,
                            }}
                        >
                            {type}
                        </Text>
                    ))}
                </View>
            </View>
        );
    };

    const renderButtons = () => {
        if (!pokemon.details) return;

        return (
            <View
                style={{
                    position: "absolute",
                    right: -25,
                    top: -25,
                }}
            >
                <TouchableOpacity
                    style={styles.button}
                    onPress={handleAddPress}
                >
                    <Icon
                        name="favorite"
                        size={25}
                        color={isInTeam ? COLORS.red : COLORS.gray}
                    />
                </TouchableOpacity>
            </View>
        );
    };

    return (
        <View style={styles.cardContainer}>
            <Text
                style={{
                    color: COLORS.white,
                    marginBottom: SIZES.base,
                    paddingBottom: SIZES.base,
                    ...FONTS.h2,
                }}
            >
                {capitalize(pokemon.name)}
            </Text>
            {renderDetails()}
            {renderButtons()}
        </View>
    );
};

export default PokemonCard;

const styles = StyleSheet.create({
    cardContainer: {
        width: "100%",
        flex: 1,
        padding: SIZES.radius,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: COLORS.primaryLight,
        marginTop: SIZES.padding,
        borderRadius: SIZES.padding,
        ...globalStyles.shadow,
    },
    button: {
        height: 50,
        width: 50,
        borderRadius: 25,
        backgroundColor: COLORS.white,
        justifyContent: "center",
        alignItems: "center",
        marginVertical: SIZES.base,
        ...globalStyles.shadow,
    },
});

import React from "react";
import { View, Text, Image } from "react-native";

import { COLORS, SIZES, globalStyles, icons, FONTS } from "../constants";

interface Props {
    children: React.ReactNode;
    alert?: boolean;
}

const Feedback = ({ children, alert }: Props) => {
    return (
        <View
            style={{
                margin: SIZES.base,
                padding: SIZES.base,
                marginTop: SIZES.base,
                backgroundColor: alert ? COLORS.blue : COLORS.primaryLight,
                borderRadius: SIZES.radius,
                flexDirection: "row",
                ...globalStyles.shadow,
            }}
        >
            <Image
                source={alert ? icons.snorlax : icons.psyduck}
                style={{ width: 50, height: 50, marginHorizontal: SIZES.base }}
            />
            <Text
                style={{
                    padding: SIZES.base,
                    color: COLORS.white,
                    flex: 1,
                    ...FONTS.h3,
                }}
            >
                {children}
            </Text>
        </View>
    );
};

export default Feedback;

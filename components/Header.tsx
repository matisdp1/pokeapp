import React from "react";
import { Text, View, Image } from "react-native";

import { FONTS, COLORS, SIZES, icons, globalStyles } from "../constants";

const Header = () => {
    return (
        <View
            style={{
                width: "100%",
                height: 60,
                backgroundColor: COLORS.green,
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "center",
                ...globalStyles.shadow,
            }}
        >
            <Text
                style={{
                    ...FONTS.h1,
                    color: COLORS.white,
                    marginLeft: SIZES.base,
                }}
            >
                P
                {
                    <Image
                        style={{ width: SIZES.h1, height: SIZES.h1 }}
                        source={icons.pokeball}
                        resizeMode="contain"
                    />
                }
                keApp
            </Text>
        </View>
    );
};

export default Header;

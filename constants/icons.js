const pokeball = require("../assets/icons/pokeball.png");
const pokeball_nav = require("../assets/icons/pokeball_nav.png");
const psyduck = require("../assets/icons/psyduck.png");
const snorlax = require("../assets/icons/snorlax.png");
const sword = require("../assets/icons/sword.png");
const shield = require("../assets/icons/shield.png");
const heart = require("../assets/icons/heart.png");

export default {
    pokeball,
    pokeball_nav,
    sword,
    shield,
    heart,
    psyduck,
    snorlax,
};

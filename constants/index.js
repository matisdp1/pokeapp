import icons from "./icons";
import theme, { COLORS, SIZES, FONTS, globalStyles } from "./theme";
import lotties from "./lotties";

export { icons, theme, COLORS, SIZES, FONTS, globalStyles, lotties };

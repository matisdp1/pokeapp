import { Dimensions, StyleSheet } from "react-native";
const { width, height } = Dimensions.get("window");

export const COLORS = {
    primary: "#333256",
    primaryLight: "#474787",

    secondary: "#B33771",

    white: "#ecf0f1",
    black: "#191919",
    ube: "#D6A2E8",
    green: "#27ae60",
    red: "#e74c3c",
    darkRed: "#6D214F",
    gray: "#95a5a6",
    blue: "#34ace0",
};
export const SIZES = {
    // global sizes
    base: 8,
    font: 14,
    radius: 12,
    padding: 24,

    // font sizes
    h1: 30,
    h2: 22,
    h3: 16,
    h4: 14,
    body1: 30,
    body2: 22,
    body3: 16,
    body4: 14,
    body5: 12,

    // app dimensions
    width,
    height,
};
export const FONTS = {
    h1: { fontFamily: "Nunito-Bold", fontSize: SIZES.h1, lineHeight: 36 },
    h2: { fontFamily: "Nunito-Bold", fontSize: SIZES.h2, lineHeight: 30 },
    h3: { fontFamily: "Nunito-Bold", fontSize: SIZES.h3, lineHeight: 22 },
    h4: { fontFamily: "Nunito-Bold", fontSize: SIZES.h4, lineHeight: 22 },
    body1: {
        fontFamily: "Poppins-Regular",
        fontSize: SIZES.body1,
        lineHeight: 36,
    },
    body2: {
        fontFamily: "Poppins-Regular",
        fontSize: SIZES.body2,
        lineHeight: 30,
    },
    body3: {
        fontFamily: "Poppins-Regular",
        fontSize: SIZES.body3,
        lineHeight: 22,
    },
    body4: {
        fontFamily: "Poppins-Regular",
        fontSize: SIZES.body4,
        lineHeight: 22,
    },
    body5: {
        fontFamily: "Poppins-Regular",
        fontSize: SIZES.body5,
        lineHeight: 22,
    },
};

export const globalStyles = StyleSheet.create({
    container: {
        backgroundColor: COLORS.primary,
        flex: 1,
        width: SIZES.width,
        paddingBottom: 80,
    },
    shadow: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 4,
        },
        shadowOpacity: 0.3,
        shadowRadius: 4.65,

        elevation: 8,
    },
});

const appTheme = { COLORS, SIZES, FONTS, globalStyles };

export default appTheme;

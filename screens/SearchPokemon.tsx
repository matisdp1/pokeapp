import React from "react";
import { View } from "react-native";

import { globalStyles } from "../constants";
import { Header, Feedback } from "../components";

interface Props {}

const SearchPokemon = () => {
    return (
        <View style={{ ...globalStyles.container }}>
            <Header />
            <Feedback>Here you can search Pokémons by name</Feedback>
            <Feedback alert={true}>
                This feature isn't available yet, please come back later
            </Feedback>
        </View>
    );
};

export default SearchPokemon;

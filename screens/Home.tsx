import React, { useEffect } from "react";
import { View, ScrollView, LogBox } from "react-native";
import { useDispatch, useSelector } from "react-redux";

import { fetchFirstsPokemons } from "../redux/features/pokeAPISlice";
import { State } from "../type";

import { COLORS, globalStyles } from "../constants";

import { PokemonList, Header, Feedback } from "../components";

const Home = () => {
    const dispatch = useDispatch();

    const { pokedexSize, pokemons, loading } = useSelector(
        (state: State) => state.pokeAPI
    );

    useEffect(() => {
        if (pokedexSize === 0) dispatch(fetchFirstsPokemons());
    }, []);

    return (
        <ScrollView style={{ backgroundColor: COLORS.primary }}>
            <View style={globalStyles.container}>
                <Header />
                <Feedback>
                    Please wait a bit to see the Pokémons that you can add to
                    your team
                </Feedback>
                <Feedback alert={true}>HELLO!</Feedback>
                <PokemonList pokemons={pokemons} loading={loading} />
            </View>
        </ScrollView>
    );
};

export default Home;

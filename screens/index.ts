import Home from "./Home";
import MyPokeTeam from "./MyPokeTeam";
import SearchPokemon from "./SearchPokemon";

export { Home, MyPokeTeam, SearchPokemon };

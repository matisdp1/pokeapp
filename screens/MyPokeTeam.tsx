import React from "react";
import { View, Text, ScrollView } from "react-native";
import { useSelector } from "react-redux";

import { COLORS, globalStyles } from "../constants";

import { State } from "../type";
import { PokemonList, Header, Feedback } from "../components";

interface Props {}

const MyPokeTeam = (props: Props) => {
    const { team } = useSelector((state: State) => state.pokeTeam);

    return (
        <ScrollView style={{ backgroundColor: COLORS.primary }}>
            <View style={globalStyles.container}>
                <Header />
                <Feedback>
                    This is your current PokeTeam, with some aditional details!
                </Feedback>
                {team.length > 0 ? (
                    <PokemonList pokemons={team} extended={true} />
                ) : (
                    <Feedback alert={true}>
                        You don't have Pokémons on your team yet
                    </Feedback>
                )}
            </View>
        </ScrollView>
    );
};

export default MyPokeTeam;

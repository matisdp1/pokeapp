import React from "react";
import { StyleSheet, Text, View, TouchableOpacity, Image } from "react-native";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import LinearGradient from "react-native-linear-gradient";
import Icon from "react-native-vector-icons/MaterialIcons";

import { Home, MyPokeTeam, SearchPokemon } from "../screens";

import { COLORS, FONTS, globalStyles, icons, SIZES } from "../constants";

interface Props {}

const Tab = createBottomTabNavigator();

const TabBarCustomButton = (props: any) => {
    return (
        <TouchableOpacity
            style={{
                top: -30,
                justifyContent: "center",
                alignItems: "center",
                ...globalStyles.shadow,
            }}
            onPress={props.onPress}
        >
            <LinearGradient
                colors={[COLORS.secondary, COLORS.darkRed]}
                style={{
                    width: 70,
                    height: 70,
                    borderRadius: 35,
                }}
            >
                {props.children}
            </LinearGradient>
        </TouchableOpacity>
    );
};

const Tabs = (props: Props) => {
    return (
        <Tab.Navigator
            tabBarOptions={{
                showLabel: false,
                style: {
                    position: "absolute",
                    bottom: 0,
                    left: 0,
                    right: 0,
                    backgroundColor: COLORS.primary,
                    borderTopColor: "transparent",
                    height: 60,
                    borderTopLeftRadius: SIZES.padding,
                    borderTopRightRadius: SIZES.padding,
                },
            }}
            initialRouteName="Home"
        >
            <Tab.Screen
                name="Search"
                component={SearchPokemon}
                options={{
                    tabBarIcon: ({ focused }) => (
                        <View
                            style={{
                                alignItems: "center",
                                justifyContent: "center",
                            }}
                        >
                            <Icon
                                name="search"
                                size={30}
                                color={focused ? COLORS.green : COLORS.white}
                            />
                            <Text
                                style={{
                                    color: focused
                                        ? COLORS.green
                                        : COLORS.white,
                                    ...FONTS.body5,
                                }}
                            >
                                SEARCH
                            </Text>
                        </View>
                    ),
                }}
            />
            <Tab.Screen
                name="Home"
                component={Home}
                options={{
                    tabBarIcon: ({ focused }) => (
                        <Icon
                            name="home"
                            size={30}
                            color={focused ? COLORS.green : COLORS.white}
                        />
                    ),
                    tabBarButton: (props) => <TabBarCustomButton {...props} />,
                }}
            />
            <Tab.Screen
                name="My Team"
                component={MyPokeTeam}
                options={{
                    tabBarIcon: ({ focused }) => (
                        <View
                            style={{
                                alignItems: "center",
                                justifyContent: "center",
                            }}
                        >
                            <Image
                                source={icons.pokeball_nav}
                                style={{
                                    height: 30,
                                    width: 30,
                                    tintColor: focused
                                        ? COLORS.green
                                        : COLORS.white,
                                }}
                            />
                            <Text
                                style={{
                                    color: focused
                                        ? COLORS.green
                                        : COLORS.white,
                                    ...FONTS.body5,
                                }}
                            >
                                MY TEAM
                            </Text>
                        </View>
                    ),
                }}
            />
        </Tab.Navigator>
    );
};

export default Tabs;

const styles = StyleSheet.create({});

import { configureStore, getDefaultMiddleware } from "@reduxjs/toolkit";
import logger from "redux-logger";

import pokeAPIReducer from "./features/pokeAPISlice";
import pokeTeamReducer from "./features/pokeTeamSlice";

const store = configureStore({
    reducer: {
        pokeAPI: pokeAPIReducer,
        pokeTeam: pokeTeamReducer,
    },
    middleware: [
        ...getDefaultMiddleware({
            immutableCheck: false,
            serializableCheck: false,
        }),
        logger,
    ],
});

export default store;

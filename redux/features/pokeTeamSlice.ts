import { createSlice, PayloadAction } from "@reduxjs/toolkit";

import { Pokemon, PokeTeamState as State } from "../../type";

const pokeTeamInitialState: State = {
    team: [],
    favorites: [],
};

// Slice

const pokeTeamSlice = createSlice({
    name: "pokeTeam",
    initialState: pokeTeamInitialState,
    reducers: {
        addToTeam: (state, { payload }: PayloadAction<Pokemon>) => {
            console.log(payload);
            state.team.push(payload);
        },
        removeFromTeam: (state, { payload }: PayloadAction<string>) => {
            const index = state.team.findIndex(
                (pokemon) => pokemon.name === payload
            );
            if (index !== -1) state.team.splice(index, 1);
        },
    },
});

// Export Actions & Reducers

export const { addToTeam, removeFromTeam } = pokeTeamSlice.actions;

export default pokeTeamSlice.reducer;

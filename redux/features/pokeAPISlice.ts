import { createSlice, createAsyncThunk, PayloadAction } from "@reduxjs/toolkit";

import { Pokemon, Details, Ability, PokeAPIState as State } from "../../type";

const pokeAPIInitialState: State = {
    pokedexSize: 0,
    nextFetchLink: "https://pokeapi.co/api/v2/pokemon/?offset=20&limit=20",
    pokemons: [],
    loading: false,
};

// Thunks

export const fetchFirstsPokemons = createAsyncThunk(
    "pokeAPI/fetchFirsts",
    async (_, thunkApi) => {
        thunkApi.dispatch(toggleLoading());

        const response = await fetch("https://pokeapi.co/api/v2/pokemon/");
        const data = await response.json();

        thunkApi.dispatch(firstRender(data));
        thunkApi.dispatch(fetchPokemonsDetails(data.results));
    }
);

export const fetchPokemonsDetails = createAsyncThunk(
    "pokeAPI/fetchDetails",
    async (pokemons: Pokemon[], thunkApi) => {
        thunkApi.dispatch(toggleLoading());

        pokemons.forEach(async (pokemon, index) => {
            if (pokemon.details?.id === undefined) {
                const apiResponse = await fetch(
                    `https://pokeapi.co/api/v2/pokemon/${pokemon.name}`
                );
                const {
                    id,
                    abilities,
                    height,
                    weight,
                    sprites,
                    stats,
                    types,
                } = await apiResponse.json();

                const details: Details = {
                    id,
                    abilities: abilities.map(
                        (ability: {
                            ability: Ability;
                            is_hidden: boolean;
                            slot: number;
                        }) => ({
                            name: ability.ability.name,
                            url: ability.ability.url,
                        })
                    ),
                    height,
                    weight,
                    image: sprites.front_default,
                    stats: stats.map(
                        (stat: {
                            base_stat: number;
                            effort: number;
                            stat: { name: string; url: string };
                        }) => ({
                            name: stat.stat.name,
                            value: stat.base_stat,
                        })
                    ),
                    types: types.map(
                        (type: {
                            slot: number;
                            type: {
                                name: string;
                                url: string;
                            };
                        }) => type.type.name
                    ),
                };

                thunkApi.dispatch(setPokemonDetails({ index, details }));
            }
        });
        thunkApi.dispatch(toggleLoading());
    }
);

// Slice

const pokeAPISlice = createSlice({
    name: "pokeAPI",
    initialState: pokeAPIInitialState,
    reducers: {
        toggleLoading: (state) => {
            state.loading = !state.loading;
        },
        firstRender: (
            state,
            {
                payload,
            }: PayloadAction<{
                count: number;
                next: string;
                results: Pokemon[];
            }>
        ) => {
            const { count, next, results } = payload;

            state.pokedexSize = count;
            state.nextFetchLink = next;
            state.pokemons = results;

            state.loading = !state.loading;
        },
        setPokemonDetails: (
            state,
            { payload }: PayloadAction<{ index: number; details: Details }>
        ) => {
            const { index, details } = payload;

            state.pokemons[index].details = details;
        },
    },
});

// Export Actions & Reducers

export const {
    toggleLoading,
    firstRender,
    setPokemonDetails,
} = pokeAPISlice.actions;

export default pokeAPISlice.reducer;
